package ec.gob.educacion.rest.seguridad;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.gob.educacion.model.DTO.RolUsuarioAplicacionDTO;
import ec.gob.educacion.model.DTO.UsuarioCorreoDTO;
import ec.gob.educacion.model.DTO.UsuarioSedeDTO;
import ec.gob.educacion.model.response.ResponseGenerico;
import ec.gob.educacion.model.seguridad.Usuario;
import ec.gob.educacion.service.seguridad.UsuarioService;

@RestController
@RequestMapping("private/")
public class UsuarioRest {

	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping(value = "actualizarCorreoUsuarioAplicacion")
	public ResponseGenerico<Usuario> actualizarCorreoUsuarioAplicacion(@RequestBody UsuarioCorreoDTO usuarioCorreoDTO) {
		Usuario respuesta = usuarioService.actualizarCorreo(usuarioCorreoDTO);
		ResponseGenerico<Usuario> response = new ResponseGenerico<>();
		response.setMensaje("Ok");
		response.setObjeto(respuesta);
		return response;
	}
	
	@PostMapping(value = "actualizarSedeUsuarioAplicacion")
	public ResponseGenerico<Usuario> actualizarSedeUsuarioAplicacion(@RequestBody UsuarioSedeDTO usuarioSedeDTO) {
		List<Usuario> listado = usuarioService.actualizarSede(usuarioSedeDTO);
		ResponseGenerico<Usuario> response = new ResponseGenerico<>();
		response.setMensaje("Ok");
		response.setListado(listado);
		return response;
	}
}
