package ec.gob.educacion.model.seguridad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "SEG_USUARIO_DETALLE_ACCION")
@SequenceGenerator(name = "SEG_USUARIO_DETALLE_ACCION_GEN", sequenceName = "SEQ_SEG_USUARIO_DETALLE_ACCION", allocationSize = 1)
public class UsuarioDetalleAccion implements java.io.Serializable {
	
	private static final long serialVersionUID = 4276997527799111216L;
	private Long codigo;
	private Usuario usuario;
	private String tipoRegistro;
	private String solicitado;
	private String ip;
	private int tipoAccion;
	private Date fechaAccion;

	public UsuarioDetalleAccion() {
	}

	public UsuarioDetalleAccion(Long codigo, Usuario usuario, String tipoRegistro, String solicitado, String ip, int tipoAccion) {
		this.codigo = codigo;
		this.usuario = usuario;
		this.tipoRegistro = tipoRegistro;
		this.solicitado = solicitado;
		this.ip = ip;
		this.tipoAccion = tipoAccion;
	}

	@Id
	@GeneratedValue(generator = "SEG_USUARIO_DETALLE_ACCION_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "CODIGO", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COD_USUARIO")
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Column(name="TIPO_REGISTRO")
	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	@Column(name="SOLICITADO")
	public String getSolicitado() {
		return solicitado;
	}

	public void setSolicitado(String solicitado) {
		this.solicitado = solicitado;
	}

	@Column(name="IP")
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name="TIPO_ACCION")
	public int getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACCION", length = 23)
	public Date getFechaAccion() {
		return fechaAccion;
	}

	public void setFechaAccion(Date fechaAccion) {
		this.fechaAccion = fechaAccion;
	}
}
