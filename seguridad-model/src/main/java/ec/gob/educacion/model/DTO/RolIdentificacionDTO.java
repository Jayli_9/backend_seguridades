package ec.gob.educacion.model.DTO;

public class RolIdentificacionDTO {
	String cedula;
	String prefijo;

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

}
