package ec.gob.educacion.model.DTO;

public class ParametroRolesDTO {
	private Integer codigoUsuario;
	private String prefijoAplicacion;
	/**
	 * @return the codigoUsuario
	 */
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	/**
	 * @param codigoUsuario the codigoUsuario to set
	 */
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	/**
	 * @return the prefijoAplicacion
	 */
	public String getPrefijoAplicacion() {
		return prefijoAplicacion;
	}
	/**
	 * @param prefijoAplicacion the prefijoAplicacion to set
	 */
	public void setPrefijoAplicacion(String prefijoAplicacion) {
		this.prefijoAplicacion = prefijoAplicacion;
	}
	
}
