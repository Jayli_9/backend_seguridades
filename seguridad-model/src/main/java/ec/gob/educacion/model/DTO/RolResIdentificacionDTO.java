package ec.gob.educacion.model.DTO;

import java.math.BigDecimal;

public class RolResIdentificacionDTO {
	private BigDecimal codigoRolApp;
	private String identificacion;
	private String rol;
	private String correo;
	private BigDecimal codigoUsuRolApp;
	private BigDecimal codigoUsuario;

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public BigDecimal getCodigoRolApp() {
		return codigoRolApp;
	}

	public void setCodigoRolApp(BigDecimal codigoRolApp) {
		this.codigoRolApp = codigoRolApp;
	}

	public BigDecimal getCodigoUsuRolApp() {
		return codigoUsuRolApp;
	}

	public void setCodigoUsuRolApp(BigDecimal codigoUsuRolApp) {
		this.codigoUsuRolApp = codigoUsuRolApp;
	}

	public BigDecimal getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(BigDecimal codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

}
