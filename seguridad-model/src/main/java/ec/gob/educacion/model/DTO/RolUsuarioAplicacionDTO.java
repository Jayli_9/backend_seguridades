package ec.gob.educacion.model.DTO;

public class RolUsuarioAplicacionDTO {
	private int codUsuario;
	private int codRolAplicacion;

	public int getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(int codUsuario) {
		this.codUsuario = codUsuario;
	}

	public int getCodRolAplicacion() {
		return codRolAplicacion;
	}

	public void setCodRolAplicacion(int codRolAplicacion) {
		this.codRolAplicacion = codRolAplicacion;
	}

}
