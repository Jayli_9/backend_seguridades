package ec.gob.educacion.model.seguridad;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "SEG_INICIO_SESION")
@SequenceGenerator(name = "SEQ_SEG_INICIO_SESION_GEN", sequenceName = "SEQ_SEG_INICIO_SESION", allocationSize = 1)
public class InicioSesion implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigo;
	private String ip;
	private String usuario;
	private String prefijoAplicacion;
	private Date fecha;

	public InicioSesion() {
	}
	

	@Id
	@GeneratedValue(generator = "SEQ_SEG_INICIO_SESION_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "CODIGO", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	@Column(name = "IP")
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "USUARIO")
	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "PREFIJO_APLICACION")
	public String getPrefijoAplicacion() {
		return prefijoAplicacion;
	}


	public void setPrefijoAplicacion(String prefijoAplicacion) {
		this.prefijoAplicacion = prefijoAplicacion;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA", length = 23)
	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
