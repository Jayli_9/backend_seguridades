package ec.gob.educacion.model.DTO;

public class UsuarioCorreoDTO {
	private Integer codigo;
	private String correo;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}
