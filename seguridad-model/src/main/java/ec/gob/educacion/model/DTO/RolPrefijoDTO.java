package ec.gob.educacion.model.DTO;

import java.math.BigDecimal;

public class RolPrefijoDTO {
	private BigDecimal codigo;
	private String descripcion;

	public BigDecimal getCodigo() {
		return codigo;
	}

	public void setCodigo(BigDecimal codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
