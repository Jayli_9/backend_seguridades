package ec.gob.educacion.model.DTO;

public class UsuarioSedeDTO {
	private String identificacion;
	private String amie;

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getAmie() {
		return amie;
	}

	public void setAmie(String amie) {
		this.amie = amie;
	}

}
