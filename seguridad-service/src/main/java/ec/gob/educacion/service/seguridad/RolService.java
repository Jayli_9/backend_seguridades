package ec.gob.educacion.service.seguridad;

import java.util.List;

import ec.gob.educacion.model.DTO.RolIdentificacionDTO;
import ec.gob.educacion.model.DTO.RolPrefijoDTO;
import ec.gob.educacion.model.DTO.RolResIdentificacionDTO;
import ec.gob.educacion.model.DTO.RolUsuarioAplicacionDTO;
import ec.gob.educacion.model.seguridad.UsuarioRolAplicacion;

public interface RolService {

	List<RolResIdentificacionDTO> listarRolesPorIdentificacionYPrefijoApp (RolIdentificacionDTO docenteDTO);
	
	List<RolPrefijoDTO> listarRolesPorPrefijoApp (String prefijo);
	
	String inactivarRolPorCodUsuRolApp (int codUsuRolApp);
	
	Integer crearUsuarioRolAplicacion (RolUsuarioAplicacionDTO rolUsuarioAplicacionDTO);
}
