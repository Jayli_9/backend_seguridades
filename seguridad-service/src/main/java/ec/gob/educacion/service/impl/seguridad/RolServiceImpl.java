package ec.gob.educacion.service.impl.seguridad;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.educacion.model.DTO.RolIdentificacionDTO;
import ec.gob.educacion.model.DTO.RolPrefijoDTO;
import ec.gob.educacion.model.DTO.RolResIdentificacionDTO;
import ec.gob.educacion.model.DTO.RolUsuarioAplicacionDTO;
import ec.gob.educacion.model.seguridad.RolAplicacion;
import ec.gob.educacion.model.seguridad.Usuario;
import ec.gob.educacion.model.seguridad.UsuarioRolAplicacion;
import ec.gob.educacion.repository.seguridad.RolAplicacionRepository;
import ec.gob.educacion.repository.seguridad.UsuarioRepository;
import ec.gob.educacion.repository.seguridad.UsuarioRolAplicacionRepository;
import ec.gob.educacion.service.seguridad.RolService;
import ec.gob.educacion.service.seguridad.UsuarioService;

@Service
public class RolServiceImpl implements RolService {

	@Autowired
	RolAplicacionRepository rolAplicacionRepository;

	@Autowired
	UsuarioRolAplicacionRepository usuarioRolAplicacionRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public List<RolResIdentificacionDTO> listarRolesPorIdentificacionYPrefijoApp(
			RolIdentificacionDTO rolIdentificacionDTO) {
		List<Object[]> resultado = rolAplicacionRepository
				.findByIdentificacionAndPrefijo(rolIdentificacionDTO.getCedula(), rolIdentificacionDTO.getPrefijo());
		List<RolResIdentificacionDTO> listado = new ArrayList<>();
		for (Object[] fila : resultado) {
			RolResIdentificacionDTO res = new RolResIdentificacionDTO();
			res.setCodigoRolApp((BigDecimal) fila[0]);
			res.setIdentificacion(fila[1].toString());
			res.setRol(fila[2].toString());
			res.setCorreo(fila[3].toString());
			res.setCodigoUsuRolApp((BigDecimal) fila[4]);
			res.setCodigoUsuario((BigDecimal) fila[5]);
			listado.add(res);
		}
		return listado;
	}

	@Override
	public List<RolPrefijoDTO> listarRolesPorPrefijoApp(String prefijo) {
		List<Object[]> resultado = rolAplicacionRepository.findByPrefijo(prefijo);
		List<RolPrefijoDTO> listado = new ArrayList<>();
		for (Object[] fila : resultado) {
			RolPrefijoDTO res = new RolPrefijoDTO();
			res.setCodigo((BigDecimal) fila[0]);
			res.setDescripcion(fila[1].toString());
			listado.add(res);
		}
		return listado;
	}

	@Override
	public String inactivarRolPorCodUsuRolApp(int codUsuRolApp) {
		UsuarioRolAplicacion usuarioRolAplicacion = usuarioRolAplicacionRepository.findByCodigo(codUsuRolApp);
		if (usuarioRolAplicacion != null) {
			usuarioRolAplicacion.setFechaFinal(new Date());
			usuarioRolAplicacionRepository.save(usuarioRolAplicacion);
			return "ok";
		}
		return "no existe el registro";
	}

	@Override
	public Integer crearUsuarioRolAplicacion(RolUsuarioAplicacionDTO rolUsuarioAplicacionDTO) {
		Usuario usuario = usuarioRepository.findByCodigo(rolUsuarioAplicacionDTO.getCodUsuario());
		RolAplicacion rol = rolAplicacionRepository.findByCodigo(rolUsuarioAplicacionDTO.getCodRolAplicacion());
		if (usuario != null) {
			if (rol != null) {
				UsuarioRolAplicacion usuarioRolAplicacion = new UsuarioRolAplicacion();
				usuarioRolAplicacion.setUsuario(usuario);
				usuarioRolAplicacion.setRolAplicacion(rol);
				usuarioRolAplicacion.setFechaInicial(new Date());
				usuarioRolAplicacion.setFechaFinal(null);
				usuarioRolAplicacionRepository.save(usuarioRolAplicacion);
				return 1;
			}
		}
		return null;
	}

}
