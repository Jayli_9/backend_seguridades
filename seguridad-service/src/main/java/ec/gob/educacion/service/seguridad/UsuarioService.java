package ec.gob.educacion.service.seguridad;

import java.math.BigDecimal;
import java.util.List;

import ec.gob.educacion.model.DTO.AplicacionDTO;
import ec.gob.educacion.model.DTO.LoginDTO;
import ec.gob.educacion.model.DTO.RolesAplicacionDTO;
import ec.gob.educacion.model.DTO.UsuarioClaveDTO;
import ec.gob.educacion.model.DTO.UsuarioCorreoDTO;
import ec.gob.educacion.model.DTO.UsuarioSedeDTO;
import ec.gob.educacion.model.seguridad.Sede;
import ec.gob.educacion.model.seguridad.Usuario;
import ec.gob.educacion.model.seguridad.UsuarioDetalleAccion;
import ec.gob.educacion.model.seguridad.UsuarioRolAplicacion;
import ec.gob.educacion.model.seguridad.Aplicacion;
import ec.gob.educacion.model.seguridad.ClaveUsuario;
import ec.gob.educacion.model.seguridad.Recurso;
import ec.gob.educacion.model.seguridad.RolAplicacion;

/**
 * Definine las operaciones disponibles en el servicio 
 */
public interface UsuarioService  {
	
	List<Usuario> buscarPorIdentificacion(String identificacion);
	
	
	List<LoginDTO> listaRespuestaLogin(String identificacion , String clave);
	
	List<RolesAplicacionDTO> listaRolesAplicaciones(Integer usuarioCod ,  String prefijoApp);
	
	List<Recurso> obtenerRecursosPorCodigoRol(Integer codigoRol);
	
	Recurso obtenerRecursoPorCodigo(Long codigoRecurso);
	
	Sede findSedeByCodigo(Long codigo);
	
	AplicacionDTO listaAplicaciones(Integer usuarioCod , String prefijoApp, Integer codigoAplicacionRol, Integer codigoAplicacion);
    
	Usuario crearUsuario(Usuario usuario);
	
	ClaveUsuario crearClaveUsuario(ClaveUsuario clave);
	
	UsuarioDetalleAccion crearUsuarioDetalleAccion(UsuarioDetalleAccion detalle);
	
	Aplicacion buscarAplicacionPorPrefijo(String prefijo);
	
	RolAplicacion buscarRolAplicacionNombre(Aplicacion aplicacion,String nombre);
	
	UsuarioRolAplicacion crearUsuarioRolAplicacion(UsuarioRolAplicacion usuarioRolAplicacion);
	
	Usuario actualizarCorreo(UsuarioCorreoDTO usuarioCorreoDTO);
	
	List<Usuario> actualizarSede(UsuarioSedeDTO usuarioSedeDTO);
}
