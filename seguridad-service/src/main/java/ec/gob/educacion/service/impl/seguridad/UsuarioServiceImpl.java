package ec.gob.educacion.service.impl.seguridad;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.educacion.model.DTO.AplicacionDTO;
import ec.gob.educacion.model.DTO.LoginDTO;
import ec.gob.educacion.model.DTO.RolesAplicacionDTO;
import ec.gob.educacion.model.DTO.UsuarioCorreoDTO;
import ec.gob.educacion.model.DTO.UsuarioSedeDTO;
import ec.gob.educacion.model.seguridad.Aplicacion;
import ec.gob.educacion.model.seguridad.ClaveUsuario;
import ec.gob.educacion.model.seguridad.Sede;
import ec.gob.educacion.model.seguridad.Usuario;
import ec.gob.educacion.model.seguridad.UsuarioDetalleAccion;
import ec.gob.educacion.model.seguridad.UsuarioRolAplicacion;
import ec.gob.educacion.model.seguridad.Recurso;
import ec.gob.educacion.model.seguridad.RolAplicacion;
import ec.gob.educacion.repository.seguridad.AplicacionRepository;
import ec.gob.educacion.repository.seguridad.ClaveRepository;
import ec.gob.educacion.repository.seguridad.RolAplicacionRepository;
import ec.gob.educacion.repository.seguridad.SedeRepository;
import ec.gob.educacion.repository.seguridad.UsuarioDetalleAccionRepository;
import ec.gob.educacion.repository.seguridad.UsuarioRepository;
import ec.gob.educacion.repository.seguridad.UsuarioRolAplicacionRepository;
import ec.gob.educacion.seguridad.resources.Constantes;
import ec.gob.educacion.service.seguridad.UsuarioService;


@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	UsuarioRepository usuarioRepository;
	@Autowired
	ClaveRepository claveRepository;
	@Autowired
	UsuarioDetalleAccionRepository usuarioDetalleAccionRepository;
	@Autowired
	AplicacionRepository aplicacionRepository;
	@Autowired
	RolAplicacionRepository rolAplicacionRepository;
	@Autowired
	UsuarioRolAplicacionRepository usuarioRolAplicacionRepository;
	@Autowired
	SedeRepository sedeRepository;
	
	List<RolesAplicacionDTO> rolesAplicacionDTO ;
	AplicacionDTO aplicacion= new AplicacionDTO();
	@Override
	public List<Usuario> buscarPorIdentificacion(String identificacion) {
		// TODO Auto-generated method stub
		return usuarioRepository.findByIdentificacionAndEstado(identificacion, Constantes.REGISTRO_ACTIVO);
	}
	
	public List<LoginDTO> listaRespuestaLogin(String identificacion, String clave) {
		List<LoginDTO> consultas = new ArrayList<>();
		usuarioRepository.listaRespuestaLogin(identificacion, clave).forEach(objects -> {
			LoginDTO cr = new LoginDTO();
			
			if(objects[0] == null || objects[0] == "" ) {
				cr.setObservacion("USUARIO O CONTRASEÑA INCORRECTOS");
				cr.setAccesoConcedido(false);
            }else{
            	cr.setCodigoUsuario(Integer.parseInt(String.valueOf(objects[0])));
            	cr.setAccesoConcedido(true);
            }
			if(objects[1] != null || objects[1] != "" ) {
				cr.setIdentificacion(String.valueOf(objects[1]));
            }
			if(objects[2] == null || objects[2] == "" ) {
            }else{
            	cr.setCedula(String.valueOf(objects[2]));
            }
			if(objects[3] == null || objects[3] == "" ) {
            }else{
            	cr.setNombre(String.valueOf(objects[3]));
            }
			if(objects[4] == null || objects[4] == "" ) {
            }else{
            	Long codigoSede=Long.parseLong(String.valueOf(objects[4]));
    			if(codigoSede!=null) {
    				cr.setSede(findSedeByCodigo(codigoSede));
    			}
            }
			
			consultas.add(cr);
		});

		return consultas;
	}
	
	@Override
	public List<RolesAplicacionDTO> listaRolesAplicaciones(Integer usuarioCod, String prefijoApp) {
		
		List<RolesAplicacionDTO> rolAplicacion = new ArrayList<>();
		
		usuarioRepository.listaRolesAplicaciones(usuarioCod, prefijoApp).forEach(objects -> {
			RolesAplicacionDTO rolAplicacionDTO = new RolesAplicacionDTO();
			
			if(objects[0] == null || objects[0] == "" ) {

            }else{
            	rolAplicacionDTO.setCodigo(Integer.parseInt(String.valueOf(objects[0])));
            }
			
			if(objects[1] == null || objects[1] == "" ) {
            }else{
            	rolAplicacionDTO.setCod_aplicacion(Integer.parseInt(String.valueOf(objects[1])));
            	List<AplicacionDTO> aplicacionAx = new ArrayList<>();
            	aplicacion = (AplicacionDTO) listaAplicaciones(usuarioCod, prefijoApp, rolAplicacionDTO.getCodigo(),rolAplicacionDTO.getCod_aplicacion());
            	rolAplicacionDTO.setAplicacion(aplicacion);
            	
            	
            }
			if(objects[2] == null || objects[2] == "" ) {
            }else{
            	rolAplicacionDTO.setNombre(String.valueOf(objects[2]));
            }
			if(objects[3] == null || objects[3] == "" ) {
            }else{
            	rolAplicacionDTO.setDescripcion(String.valueOf(objects[3]));
            }
			if(objects[4] == null || objects[4] == "" ) {
				
            }else{
            	
            	rolAplicacionDTO.setEstado(String.valueOf(objects[4]));
            	
            }

			
			rolAplicacion.add(rolAplicacionDTO);			
		});

		return rolAplicacion;
	}


	@Override
	public Sede findSedeByCodigo(Long codigo) {
		return usuarioRepository.busquedarSedePorCodigo(codigo);
	}

	@Override
	public AplicacionDTO listaAplicaciones(Integer usuarioCod, String prefijoApp, Integer codigo,
			Integer codigoAplicacion) {
		// TODO Auto-generated method stub
		return usuarioRepository.listaAplicaciones(usuarioCod, prefijoApp, codigo, codigoAplicacion);
	}
		
	@Override
	public List<Recurso> obtenerRecursosPorCodigoRol(Integer codigoRol) {	
		return usuarioRepository.listaMenu(codigoRol);
	}
	
	@Override
	public Recurso obtenerRecursoPorCodigo(Long codigoRecurso) {
		return usuarioRepository.obtenerRecurso(codigoRecurso);
	}


    @Override
    public Usuario crearUsuario(Usuario usuario) {
    	return usuarioRepository.save(usuario);
    }
    @Override
    public ClaveUsuario crearClaveUsuario(ClaveUsuario clave) {
    	return claveRepository.save(clave);
    }
    @Override
    public UsuarioDetalleAccion crearUsuarioDetalleAccion(UsuarioDetalleAccion detalle) {
    	return usuarioDetalleAccionRepository.save(detalle);
    }
    @Override
    public Aplicacion buscarAplicacionPorPrefijo(String prefijo) {
    	return aplicacionRepository.findByPrefijoAndEstado(prefijo,Constantes.REGISTRO_ACTIVO);
    }
    @Override
    public RolAplicacion buscarRolAplicacionNombre(Aplicacion aplicacion, String nombre) {
    	return rolAplicacionRepository.findByAplicacionAndNombreRolAndEstadoRol(aplicacion, nombre, Constantes.REGISTRO_ACTIVO);
    }
    @Override
    public UsuarioRolAplicacion crearUsuarioRolAplicacion(UsuarioRolAplicacion usuarioRolAplicacion) {
    	return usuarioRolAplicacionRepository.save(usuarioRolAplicacion);
    }

	@Override
	public Usuario actualizarCorreo(UsuarioCorreoDTO usuarioCorreoDTO) {
		Usuario usuario = usuarioRepository.findByCodigo(usuarioCorreoDTO.getCodigo());
		if (usuario != null) {
			usuario.setCorreoElectronico(usuarioCorreoDTO.getCorreo());
			return usuarioRepository.save(usuario);
		}
		return null;
	}

	@Override
	public List<Usuario> actualizarSede(UsuarioSedeDTO usuarioSedeDTO) {
		Sede sede = sedeRepository.findByNemonicoAndEstado(usuarioSedeDTO.getAmie(), "A");
		List<Usuario> usuarios = usuarioRepository.findByIdentificacionAndEstado(usuarioSedeDTO.getIdentificacion(), "A");
		for (Usuario usuario : usuarios) {
			usuario.setSede(sede);
			usuarioRepository.save(usuario);
		}
		return usuarios;
	}
   
}
