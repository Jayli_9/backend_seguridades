package ec.gob.educacion.repository.seguridad;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.DTO.AplicacionDTO;
import ec.gob.educacion.model.seguridad.Recurso;
import ec.gob.educacion.model.seguridad.Sede;
import ec.gob.educacion.model.seguridad.Usuario;


@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{
	
	List<Usuario>  findByIdentificacionAndEstado(@Param("identificacion") String identificacion,@Param("estado") String estado);
	
	List<Usuario> findByIdentificacion(String identificacion);

	@Query(nativeQuery = true , value= "SELECT \r\n" + 
			"U.CODIGO,\r\n" + 
			"U.IDENTIFICACION,\r\n" + 
			"C.CLAVE,\r\n" +
			"U.ESTADO, \r\n" +
			"CONCAT(U.APELLIDOS,U.NOMBRES) as NOMBRE, \r\n" + 
			"U.CODIGO_SEDE \r\n"+
			"FROM \r\n" + 
			"CUPOS.SEG_USUARIO U, CUPOS.SEG_CLAVE_USUARIO C \r\n" + 		
			"WHERE\r\n" + 
			"U.IDENTIFICACION = :identificacion AND\r\n" + 
			"C.CLAVE = :clave AND\r\n" + 
			"C.ESTADO = 'A' AND\r\n" +
			"U.CODIGO = C.COD_USUARIO and \r\n" + 			
			"U.ESTADO = 'A'")
	List<Object[]> listaUsuario(@Param("identificacion") String identificacion , @Param("clave") String clave);
	
	@Query(nativeQuery = true , value= "SELECT\r\n" + 
			"	U.CODIGO,\r\n" + 
			"	U.IDENTIFICACION,\r\n" + 
			"	U.CEDULA,\r\n" + 
			"	CONCAT(U.APELLIDOS,U.NOMBRES) as NOMBRE,\r\n" + 
			"	U.CODIGO_SEDE\r\n " + 
			"FROM\r\n" + 
			"	CUPOS.SEG_USUARIO U\r\n" + 
			"	INNER JOIN CUPOS.SEG_CLAVE_USUARIO C ON U.CODIGO = C.COD_USUARIO\r\n" + 
			"WHERE\r\n" + 
			"	U.IDENTIFICACION = :identificacion\r\n" + 
			"	AND C.CLAVE = :clave\r\n" + 
			"	AND C.ESTADO = 'A'\r\n" + 
			"	AND U.ESTADO = 'A'")
	List<Object[]> listaRespuestaLogin(@Param("identificacion") String identificacion , @Param("clave") String clave);
	
	
	@Query(nativeQuery = false , value= "select a.recurso from RolAplicacionRecurso a where a.rolAplicacion.codigo = :codigoRol and a.estado = 'A'")
	List<Recurso> listaMenu(@Param("codigoRol") Integer codigoRol);
	
	@Query(nativeQuery = false , value= "select a from Recurso a where a.codigo = :codigoRecurso")
	Recurso obtenerRecurso(@Param("codigoRecurso") Long codigoRecurso);	
	
	@Query(value= "  SELECT new ec.gob.educacion.model.DTO.AplicacionDTO( a.codigo,a.prefijo, a.nombre, a.descripcion, a.url, a.estado,\r\n"
			+ "  a.tipo, r.descripcionRol, r.estadoRol, r.nombreRol) from Aplicacion a\r\n"
			+ "  inner join RolAplicacion r on r.aplicacion = a.codigo\r\n"
			+ "  inner join UsuarioRolAplicacion u on u.rolAplicacion = r.codigo\r\n"
			+ " inner join Usuario s on s.codigo = u.usuario"
			+ "  where a.estado='A'\r\n"
			+ "  and r.estadoRol ='A'\r\n"
			+ "  and r.aplicacion = a.codigo\r\n"
			+ "  and a.prefijo =:prefijoApp\r\n"
			+ "  and s.codigo  =:usuarioCod\r\n"
			+ "  and r.codigo =:codigoAplicacionRol\r\n"
			+ "  and a.codigo =:codigoAplicacion and u.fechaFinal is null")
	AplicacionDTO listaAplicaciones(@Param("usuarioCod") Integer usuarioCod , @Param("prefijoApp") String prefijoApp,@Param("codigoAplicacionRol") Integer codigoAplicacionRol,@Param("codigoAplicacion") Integer codigoAplicacion);
	
	
	@Query(nativeQuery = true , value= "SELECT SEG_ROL_APLICACION.CODIGO, SEG_ROL_APLICACION.COD_APLICACION, SEG_ROL_APLICACION.NOMBRE, \r\n" + 
			"SEG_ROL_APLICACION.DESCRIPCION, SEG_ROL_APLICACION.ESTADO, SEG_ROL_APLICACION.ENUM_ROL_SEG, SEG_ROL_APLICACION.FEC_INACTIVACION \r\n" + 
			"FROM SEG_ROL_APLICACION, SEG_APLICACION, SEG_USUARIO_ROL_APLICACION\r\n" + 
			"WHERE SEG_APLICACION.PREFIJO=:prefijoApp \r\n" + 
			"AND SEG_APLICACION.ESTADO ='A'\r\n" + 
			"AND SEG_ROL_APLICACION.COD_APLICACION = SEG_APLICACION.CODIGO \r\n" + 
			"AND SEG_ROL_APLICACION.ESTADO = 'A'\r\n" + 
			"AND SEG_USUARIO_ROL_APLICACION.COD_USUARIO = :usuarioCod\r\n" + 
			"AND SEG_USUARIO_ROL_APLICACION.COD_ROL_APLICACION = SEG_ROL_APLICACION.CODIGO\r\n" +
			"AND SEG_USUARIO_ROL_APLICACION.FECHA_FINAL IS NULL")
	List<Object[]> listaRolesAplicaciones(@Param("usuarioCod") Integer usuarioCod , @Param("prefijoApp") String prefijoApp);

	
	@Query(value= "SELECT c FROM Sede c where c.codigo= :codigo" )
	Sede busquedarSedePorCodigo(@Param("codigo") Long codigo);
	
	Usuario findByCodigo(int codigo);
}
