package ec.gob.educacion.repository.seguridad;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.seguridad.Aplicacion;
import ec.gob.educacion.model.seguridad.RolAplicacion;

@Repository
public interface RolAplicacionRepository extends JpaRepository<RolAplicacion, Integer> {

	RolAplicacion findByAplicacionAndNombreRolAndEstadoRol(@Param("aplicacion") Aplicacion aplicacion,
			@Param("nombreRol") String nombre, @Param("estadoRol") String estado);

	@Query(nativeQuery = true, value = "SELECT\r\n" + "CUPOS.SEG_ROL_APLICACION.CODIGO AS COD_ROL_APP,\r\n"
			+ "CUPOS.SEG_USUARIO.IDENTIFICACION,\r\n" + "CUPOS.SEG_ROL_APLICACION.DESCRIPCION,\r\n"
			+ "CUPOS.SEG_USUARIO.CORREO_ELECTRONICO,\r\n"
			+ "CUPOS.SEG_USUARIO_ROL_APLICACION.CODIGO AS COD_USU_ROL_APP,\r\n"
			+ "CUPOS.SEG_USUARIO.CODIGO AS COD_USUARIO FROM\r\n" + "CUPOS.SEG_USUARIO\r\n"
			+ "INNER JOIN CUPOS.SEG_USUARIO_ROL_APLICACION ON CUPOS.SEG_USUARIO.CODIGO = CUPOS.SEG_USUARIO_ROL_APLICACION.COD_USUARIO\r\n"
			+ "INNER JOIN CUPOS.SEG_ROL_APLICACION ON CUPOS.SEG_USUARIO_ROL_APLICACION.COD_ROL_APLICACION = CUPOS.SEG_ROL_APLICACION.CODIGO\r\n"
			+ "INNER JOIN CUPOS.SEG_APLICACION on CUPOS.SEG_ROL_APLICACION.COD_APLICACION = CUPOS.SEG_APLICACION.CODIGO\r\n"
			+ "WHERE \r\n" + "CUPOS.SEG_APLICACION.PREFIJO = :prefijo AND\r\n"
			+ "CUPOS.SEG_USUARIO_ROL_APLICACION.FECHA_FINAL IS NULL AND\r\n"
			+ "CUPOS.SEG_USUARIO.IDENTIFICACION = :identificacion")
	List<Object[]> findByIdentificacionAndPrefijo(@Param("identificacion") String identificacion,
			@Param("prefijo") String prefijo);

	@Query(nativeQuery = true, value = "SELECT\r\n" + "CUPOS.SEG_ROL_APLICACION.CODIGO,\r\n"
			+ "CUPOS.SEG_ROL_APLICACION.DESCRIPCION\r\n" + "FROM\r\n" + "CUPOS.SEG_ROL_APLICACION \r\n"
			+ "INNER JOIN CUPOS.SEG_APLICACION on CUPOS.SEG_ROL_APLICACION.COD_APLICACION = CUPOS.SEG_APLICACION.CODIGO\r\n"
			+ "WHERE CUPOS.SEG_APLICACION.PREFIJO = :prefijo")
	List<Object[]> findByPrefijo(@Param("prefijo") String prefijo);
	
	RolAplicacion findByCodigo(int codigo);
}
