package ec.gob.educacion.repository.seguridad;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.seguridad.Aplicacion;

@Repository
public interface AplicacionRepository extends JpaRepository<Aplicacion, Integer>{
  Aplicacion findByPrefijoAndEstado(@Param("prefijo") String prefijo,@Param("estado") String estado);
}
