package ec.gob.educacion.repository.seguridad;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.seguridad.Sede;

@Repository
public interface SedeRepository extends JpaRepository<Sede, Long> {
	Sede findByNemonicoAndEstado(String nemonico, String estado);
}
