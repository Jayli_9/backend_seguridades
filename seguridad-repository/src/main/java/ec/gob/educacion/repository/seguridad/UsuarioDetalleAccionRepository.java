package ec.gob.educacion.repository.seguridad;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.seguridad.UsuarioDetalleAccion;

@Repository
public interface UsuarioDetalleAccionRepository extends JpaRepository<UsuarioDetalleAccion, Long>{

}
