package ec.gob.educacion.repository.seguridad;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.seguridad.UsuarioRolAplicacion;

@Repository
public interface UsuarioRolAplicacionRepository extends JpaRepository<UsuarioRolAplicacion, Integer> {
	UsuarioRolAplicacion findByCodigo(int codigo);

}
