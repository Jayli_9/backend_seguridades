package ec.gob.educacion.repository.seguridad;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.gob.educacion.model.seguridad.ClaveUsuario;

@Repository
public interface ClaveRepository extends JpaRepository<ClaveUsuario, Long>{

}
